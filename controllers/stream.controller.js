const fs = require('fs');
const spawn = require('child_process').spawn;
const path = require('path');

// Create a new stream
exports.newStream = (req, res) => {
    const rtspUrl = req.body.rtspUrl;
    const cameraName = req.body.cameraName;
    const streamUrl = `http://localhost:3000/stream/${cameraName}`;

    if(!fs.existsSync(`public/${cameraName}`)) {
        fs.mkdirSync(`public/${cameraName}`);
        if(!fs.existsSync(`public/${cameraName}/segments`)) {
            fs.mkdirSync(`public/${cameraName}/segments`);
        }
    }
    res.json({ streamUrl });
    
    const ffmpegProcess = spawn('ffmpeg', [
        '-fflags', 'nobuffer',
        '-rtsp_transport', 'tcp',
        '-i', rtspUrl,
        '-vsync', '0',
        '-copyts',
        '-vcodec', 'copy',
        '-preset', 'ultrafast',
        '-movflags', 'frag_custom+empty_moov+default_base_moof',
        '-an',
        '-hls_flags', 'delete_segments+append_list',
        '-f', 'segment',
        '-segment_list_flags', 'live',
        '-segment_time', '2',
        '-segment_list_size', '3',
        '-segment_format', 'mpegts',
        '-segment_list', `public/${cameraName}/index.m3u8`,
        '-segment_list_type', 'm3u8',
        '-segment_list_entry_prefix', `/${cameraName}/segments/`,
        '-strftime', '1',
        `public/${cameraName}/segments/%d-%m-%Y_%H:%M:%S.ts`
      ]);
      

    ffmpegProcess.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`);
    });

    ffmpegProcess.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
    });

    ffmpegProcess.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
};

// Stop one stream by code
exports.stopStream = (req, res) => {
    const code = req.body.code;

    const ffmpegProcess = spawn('kill', [code]);
    ffmpegProcess.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`);
    });

    ffmpegProcess.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
    });

    ffmpegProcess.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
}

// Stop all streams
exports.stopAllStream = (req, res) => {
    res.json({ message: 'Stream stopped' });
    const ffmpegProcess = spawn('killall', ['ffmpeg']);
    ffmpegProcess.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`);
    });

    ffmpegProcess.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
    });

    ffmpegProcess.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
}

// Get stream
exports.getStream = (req, res) => {
    const fileName = 'index.m3u8';
    const filePath = path.join(__dirname, '../public', `${req.params.cameraName}/${fileName}`);
    res.header('Access-Control-Allow-Origin', '*').sendFile(filePath)
}