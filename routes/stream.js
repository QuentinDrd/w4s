const express = require('express');
const router = express.Router();

const streamController = require('../controllers/stream.controller');

router.get('/', (req, res) => {console.log('test')} );

router.post('/create-stream', streamController.newStream);

router.post('/stop-all-stream', streamController.stopAllStream);

router.get('/:cameraName', streamController.getStream);

module.exports = router;