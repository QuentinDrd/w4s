const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();

const streamRouter = require('./routes/stream');
const publicPath = path.join(__dirname, 'public');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(publicPath));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.use('/stream', streamRouter);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});


app.listen(3000, () => {
    console.log('Le serveur est en écoute sur le port 3000');
});


